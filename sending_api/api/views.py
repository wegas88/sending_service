from django.db.models import Count, Q, F
from django.shortcuts import render, redirect
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from api.models import Client, Sending, Message
from api.serializers import ClientSerializer, SendingDetailSerializer, \
    SendingSerializer, MessageSerializer


class ClientViewSet(ModelViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()


class SendingViewSet(ModelViewSet):
    serializer_class = SendingSerializer
    queryset = Sending.objects.all()

    @action(detail=False, methods=['get'])
    def statistics(self, request, *args, **kwargs):
        total_messages_all = Message.objects.count()

        statistics = Sending.objects.order_by('pk').annotate(
            total_messages=Count('messages'),
            sent_messages=Count('messages', filter=Q(messages__status=Message.Statuses.SENT)),
            unsent_messages=Count('messages', filter=Q(messages__status=Message.Statuses.UNSENT))
        ).values('pk', 'total_messages', 'sent_messages', 'unsent_messages')

        data = {
            "Total messages": total_messages_all,
            "Sendings": statistics,
        }

        return Response(data)

    @action(detail=True, methods=["get"])
    def full_info(self, request, pk=None):
        sending = get_object_or_404(Sending, pk=pk)
        serializer = SendingDetailSerializer(sending)
        return Response(serializer.data)
