from rest_framework import serializers
from django.utils import timezone
from api.models import Client, Sending, Message


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'phone', 'operator_code', 'tag', 'timezone']


class SendingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sending
        fields = ['id',
                  'start_datetime',
                  'end_datetime',
                  'text',
                  'operator_code',
                  'tag',
                  ]


class MessageSerializer(serializers.ModelSerializer):
    client = ClientSerializer(read_only=True)

    class Meta:
        model = Message
        fields = ['id', 'created_at', 'status', 'client']


class SendingDetailSerializer(serializers.ModelSerializer):
    messages = MessageSerializer(many=True, read_only=True)

    class Meta:
        model = Sending
        fields = ['id', 'start_datetime', 'end_datetime', 'text', 'operator_code', 'tag', 'messages']
