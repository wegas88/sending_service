import logging

from api.models import Sending, Client, Message
from api.utils import send_message_to_external_api
from sending_api.celery import app

logger = logging.getLogger(__name__)


@app.task(bind=True, retry_backoff=True, default_retry_delay=20)
def send_message_to_client(self, message_id):
    logging.info("Start sending")
    try:
        message = Message.objects.get(pk=message_id)
        try:
            status_code = send_message_to_external_api(message.pk, message.sending.text, message.client.phone)
            if status_code == 200:
                message.status = Message.Statuses.SENT
                message.save()
            else:
                raise self.retry()
        except Exception as exc:
            logger.error(f"Error sending message: {exc}")
            raise self.retry(exc=exc)
        logging.info("Message sent")
    except Message.DoesNotExist:
        logging.error("Message does not exist")
