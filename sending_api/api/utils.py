import logging

import requests
from django.conf import settings

logger = logging.getLogger(__name__)


def send_message_to_external_api(message_pk, text, phone) -> int:
    url = f'{settings.EXTERNAL_API_URL}/{message_pk}'

    headers = {
    'accept': 'application/json',
    'Authorization': f'Bearer {settings.API_TOKEN}',
    'Content-Type': 'application/json',
    }

    data = {
      "id": int(message_pk),
      "phone": int(phone),
      "text": text
    }

    logger.info(f"Sending to external API. Phone: {phone}")
    response = requests.post(url, headers=headers, json=data)

    if response.status_code == 200:
        logger.info(f"[200] Sended to API. Phone: {phone}.\n {data=}")
    else:
        logger.info(f"[{response.status_code}] while sending to API. Phone: {phone}.\n {data=}")
        logger.info(response.text)
    return response.status_code
