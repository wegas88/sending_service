import pytz
from django.db import models


class Sending(models.Model):
    start_datetime = models.DateTimeField(verbose_name='Дата и время запуска рассылки')
    end_datetime = models.DateTimeField(verbose_name='Дата и время окончания рассылки')
    text = models.TextField(verbose_name='Текст сообщения для доставки клиенту')
    operator_code = models.CharField(verbose_name='Фильтр: код мобильного оператора', null=True, blank=True)
    tag = models.CharField(verbose_name='Фильтр: тег', null=True, blank=True)

    def __str__(self):
        return f'{self.start_datetime} - {self.end_datetime}'

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'


class Client(models.Model):
    TIMEZONES_CHOICES = tuple([(timezone, timezone) for timezone in pytz.all_timezones])
    phone = models.CharField(max_length=11, verbose_name='Номер телефона', unique=True)
    operator_code = models.CharField(max_length=3, verbose_name='Код оператора', editable=False)
    tag = models.CharField(max_length=100, verbose_name='Тег', null=True, blank=True)
    timezone = models.CharField(max_length=50, default='UTC', choices=TIMEZONES_CHOICES)

    def save(self, *args, **kwargs):
        self.operator_code = str(self.phone)[1:4]
        return super(Client, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.phone}'

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class Message(models.Model):
    class Statuses(models.TextChoices):
        SENT = 'SENT', 'Отправлено'
        UNSENT = 'UNSENT', 'Не отправлено'

    created_at = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=10, choices=Statuses.choices, default=Statuses.UNSENT)
    sending = models.ForeignKey(Sending, on_delete=models.CASCADE, related_name='messages')
    client = models.ForeignKey(Client, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.client}'

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'