from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import Q
from django.utils import timezone

from api.models import Sending, Client, Message
from api.tasks import send_message_to_client


@receiver(post_save, sender=Sending)
def create_message(sender, instance, created, **kwargs):
    if created:
        clients = Client.objects.filter(Q(operator_code=instance.operator_code) | Q(tag=instance.tag))

        if clients:
            msgs_objects = [Message(sending=instance, client=client) for client in clients]
            messages = Message.objects.bulk_create(msgs_objects)
            for message in messages:
                now = timezone.now()
                if instance.start_datetime <= now <= instance.end_datetime:
                    send_message_to_client.apply_async(
                        (message.pk,), expires=instance.end_datetime
                    )
                else:
                    send_message_to_client.apply_async(
                        (message.pk,),
                        eta=instance.start_datetime,
                        expires=instance.end_datetime,
                    )
