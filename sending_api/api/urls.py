from django.urls import path, include
from rest_framework import routers

from api.views import ClientViewSet, SendingViewSet

router = routers.DefaultRouter()
router.register(r'clients', ClientViewSet)
router.register(r'sendings', SendingViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
