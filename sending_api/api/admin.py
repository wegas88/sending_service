from django.contrib import admin

from api.models import Client, Sending, Message


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'tag', 'operator_code']


@admin.register(Sending)
class SendingAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'text', 'operator_code', 'tag']


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'status', 'created_at']
    list_filter = ['status', 'created_at', 'sending__start_datetime', 'sending__end_datetime', 'sending']
