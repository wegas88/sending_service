## Сервис уведомлений
Тестовое задание, проект разработан на Django + DRF + Celery

Установка проекта с помощью docker-compose

Клонировать репозиторий с Gitlab

`git clone https://gitlab.com/wegas88/sending_service.git`

Перейти в директорию проекта
`cd <existing_repo>/sending_api`

Создать файл .env и заполнить необходимые данные (пример в .env.example)



Сбилдить образ проекта

`docker build . -t sending_api`

Запустить контейнеры

`docker-compose up -d`

Остановка работы контейнеров

`docker-compose stop`

http://127.0.0.1:8000/api/ - API URL

http://127.0.0.1:8000/api/clients/ - Эндпоинт клиентов

http://127.0.0.1:8000/api/sendings/ - Эндпоинт рассылок

http://127.0.0.1:8000/api/sendings/statistics/ - Общая статистика по рассылкам

http://127.0.0.1:8000/api/sendings/<pk>/full_info/ - Детальная статистика по конкретной рассылке

http://127.0.0.1:8000/docs/ - Документация API

Для доступа в админку выполнить команду:
`docker exec -it sending_api_web python manage.py createsuperuser --noinput`

Логин и пароль будет задан из .env (DJANGO_SUPERUSER_USERNAME, DJANGO_SUPERUSER_PASSWORD)